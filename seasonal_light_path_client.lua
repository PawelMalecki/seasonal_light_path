seasonal_light_path = {}

local monkey_patches = {
   seasonal_light_path_sky_renderer_service = 'stonehearth.services.client.sky_renderer.sky_renderer_service'
}

local function monkey_patching()
   for from, into in pairs(monkey_patches) do
      local monkey_see = require('monkey_patches.' .. from)
      local monkey_do = radiant.mods.require(into)
      radiant.log.write_('seasonal_light_path', 0, 'Seasonal Light Path client monkey-patching sources \'' .. from .. '\' => \'' .. into .. '\'')
      radiant.mixin(monkey_do, monkey_see)
   end
end

function seasonal_light_path:_on_required_loaded()
   monkey_patching()
   radiant.events.trigger_async(radiant, 'seasonal_light_path:client:required_loaded')
end

radiant.events.listen(radiant, 'radiant:required_loaded', seasonal_light_path, seasonal_light_path._on_required_loaded)

return seasonal_light_path