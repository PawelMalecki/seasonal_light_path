local SkyRenderer = require 'stonehearth.services.client.sky_renderer.sky_renderer_service'

local time_constants = radiant.resources.load_json('/stonehearth/data/calendar/calendar_constants.json')
time_constants.days_per_year = time_constants.months_per_year * time_constants.days_per_month

local SeasonalSkyRenderer = class()

SeasonalSkyRenderer._seasonal_old_on_server_ready = SkyRenderer.on_server_ready
function SeasonalSkyRenderer:on_server_ready()
   self:update_biome_constants()
   self:_seasonal_old_on_server_ready()
end

function SeasonalSkyRenderer:update_biome_constants()
   _radiant.call('seasonal_light_path:get_biome_alias'):done(function(e)
      if not (self._biome_constants and self._biome_constants.uri and self._biome_constants.uri == e.biome_uri) then
         local biome_constants = radiant.resources.load_json('seasonal_light_path:biome_constants')
         self._biome_constants = biome_constants[e.biome_uri] or biome_constants['stonehearth:biome:temperate']
         self._biome_constants.uri = e.biome_uri
         if self._sky_settings then self._sky_settings = self:daily_angle_recalc(self._sky_settings) end
      end
   end)
end

SeasonalSkyRenderer._seasonal_old_update_time = SkyRenderer._update_time
function SeasonalSkyRenderer:_update_time(seconds)
   local should_recalc = false
   if self._clock_object and self._sky_settings then
      local date = self._clock_object:get_data()
      if not self._date or (self._date.day ~= date.day or self._date.month ~= date.month) then
         self._date = {day = date.day, month = date.month, year = date.year}
         should_recalc = true
      end
   end
   if not (self._biome_constants and self._biome_constants.uri and self._biome_constants.uri ~= '') then
      self:update_biome_constants()
   end
   if should_recalc then
      self._sky_settings = self:daily_angle_recalc(self._sky_settings)
   end
   self:_seasonal_old_update_time(seconds)
end

SeasonalSkyRenderer._seasonal_old_parse_sky_settings = SkyRenderer.parse_sky_settings
function SeasonalSkyRenderer:parse_sky_settings(sky_settings)
   if not sky_settings.disable_seasonal_path then
      return self:daily_angle_recalc(self:_seasonal_old_parse_sky_settings(sky_settings))
   end
   return self:_seasonal_old_parse_sky_settings(sky_settings)
end

function SeasonalSkyRenderer:daily_angle_recalc(sky_settings)
   if sky_settings.disable_seasonal_path or not (self._date and sky_settings.celestials) then
      return sky_settings
   end

   if not self._biome_constants then
      self._biome_constants = {latitude = 42, north = -30, uri = ''}
   end

   local declination = self:_get_declination(self._date.day, self._date.month)
   local peak_altitude = self:_get_peak_altitude(declination)
   local sunrise_azimuth = self:_get_sunrise_azimuth(declination)

   if sky_settings.celestials.sun then
      for _, entry in ipairs(sky_settings.celestials.sun[2]) do
         entry[2].x = self:_get_sun_altitude(entry[1], peak_altitude)
         entry[2].y = self:_get_sun_azimuth(entry[1], sunrise_azimuth)
         entry[2].z = 0
         entry[2].w = 0
      end
   end

   if sky_settings.celestials.moon then
      local moonrise_altitude = self:_get_sun_altitude(self.timing.sunset_peak_start, peak_altitude)
      local moonrise_azimuth = self:_get_sun_azimuth(self.timing.sunset_peak_start, sunrise_azimuth) + 180
      local moonset_altitude = self:_get_sun_altitude(self.timing.sunrise_peak_end, peak_altitude)
      local moonset_azimuth = self:_get_sun_azimuth(self.timing.sunrise_peak_end, sunrise_azimuth) + 180
      for _, entry in ipairs(sky_settings.celestials.moon[2]) do
         if entry[1] < self.timing.midday then
            entry[2].x = self.deegrify(moonset_altitude - (math.deg(peak_altitude) + moonset_altitude) * self:_get_night_distribution(entry[1]))
         else
            entry[2].x = self.deegrify(moonrise_altitude - (math.deg(peak_altitude) + moonrise_altitude) * self:_get_night_distribution(entry[1]))
         end
         entry[2].y = self.deegrify(moonrise_azimuth + (moonset_azimuth - moonrise_azimuth) * self:_get_night_fraction(entry[1]))
         entry[2].z = 0
         entry[2].w = 0
      end
   end

   return sky_settings
end
function SeasonalSkyRenderer.deegrify(value)
   while value > 360 do
      value = value - 360
   end
   while value < -360 do
      value = value + 360
   end
   return value
end

function SeasonalSkyRenderer:_get_declination(day, month)
  -- first we must determine in-game winter solstice date which IRL is at (365-10)/365 = 0.9726 of the year length for northern hemisphere and 0.5 less for southern (0.4726)
   local solstice_frac = 0.9726
   if self._biome_constants.latitude < 0 then
     solstice_frac = solstice_frac - 0.5
   end
   -- then we must get the fraction for the current date to compare the two
   local date_frac = (month * time_constants.days_per_month + day)/time_constants.days_per_year
   -- usually the formula given is based on number of days since solstice but as we don't define the exact solstice date we'll stick to fractions here (we'd have to divide by days_per_year in the end anyway)
   -- now we'll simply phase-shift the cosine by the value of solstice_frac (this actually assumes the Earth orbits the Sun in a perfect circle what is not exactly true!)
   -- the last step is to multiply the result by the angle of Earth's rotation axis tilt which is about -23.45 degrees = -0.409 rad
   return -0.409 * math.cos(2 * math.pi * (date_frac + solstice_frac))
end

function SeasonalSkyRenderer:_get_peak_altitude(declination)
  -- 1.57079633 rad = 90 deg
   return 1.57079633 - math.abs(math.rad(self._biome_constants.latitude)) + declination
end

function SeasonalSkyRenderer:_get_sunrise_azimuth(declination)
   return math.acos(math.sin(declination)/math.cos(math.rad(self._biome_constants.latitude)))
end

function SeasonalSkyRenderer:_get_day_fraction(seconds)
   if seconds > self.timing.sunrise_start then
      return (seconds - self.timing.sunrise_start)/(self.timing.day_length - self.timing.sunrise_start)
   else
      return 0
   end
end

function SeasonalSkyRenderer:_get_day_distribution(seconds)
   if seconds == self.timing.midday then
      return 1
   elseif seconds <= self.timing.sunrise_start or seconds >= self.timing.day_length then
      return 0
   end
   return math.min((seconds - self.timing.sunrise_start)/(self.timing.midday - self.timing.sunrise_start),(self.timing.day_length - seconds)/(self.timing.day_length - self.timing.midday))
end

function SeasonalSkyRenderer:_get_night_fraction(seconds)
   if seconds < self.timing.sunset_peak_start then
      seconds = seconds + self.timing.day_length
   end
   return (seconds - self.timing.sunset_peak_start)/(self.timing.day_length + self.timing.sunrise_peak_end - self.timing.sunset_peak_start)
end

function SeasonalSkyRenderer:_get_night_distribution(seconds)
   if seconds == self.timing.midnight or seconds == self.timing.day_length then
      return 1
   elseif seconds >= self.timing.sunrise_peak_end and seconds <= self.timing.sunset_peak_start then
      return 0
   elseif seconds < self.timing.midday then
      seconds = seconds + self.timing.day_length
   end
   return math.min((seconds - self.timing.sunset_peak_start)/(self.timing.day_length - self.timing.sunset_peak_start),(self.timing.day_length + self.timing.sunrise_peak_end - seconds)/self.timing.day_length)
end

function SeasonalSkyRenderer:_get_sun_altitude(seconds, peak_altitude)
   return self.deegrify(-math.deg(peak_altitude) * self:_get_day_distribution(seconds))
end

function SeasonalSkyRenderer:_get_sun_azimuth(seconds, sunrise_azimuth)
   local north = self._biome_constants.north or -30
   return self.deegrify(math.deg(sunrise_azimuth) * (1 - 2 * self:_get_day_fraction(seconds)) - north)
end

return SeasonalSkyRenderer