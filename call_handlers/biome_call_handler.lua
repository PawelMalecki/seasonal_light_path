local BiomeCallHandler = class()

function BiomeCallHandler:get_biome_alias(session, request)
   if stonehearth.world_generation then
      return { biome_uri = stonehearth.world_generation:get_biome_alias() }
   else
      return { biome_uri = 'stonehearth:biome:temperate' }
   end
end

return BiomeCallHandler
